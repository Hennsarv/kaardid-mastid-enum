﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaardidMastidEnummid
{
    // õpime ühe lisaviguri - enum
    // me oleme teda (enese teadmata) natuke kasutanud ka juba
    // kui me ütlesime DayOfWeek.Saturday

    enum Mast { Risti, Ruutu, Ärtu, Poti }
    enum Kaart { Kaks=2, Kolm, Neli, Viis, Kuus, Seitse, Kaheksa, Üheksa, Kümme, Soldat, Emand, Kuningas, Äss }

    static partial class Program
    {

        public static void Main()
        {
            Console.Write("Kas variant 1 või 2: ");
            switch(Console.ReadLine())
            {
                case "1": Main1(); break;
                case "2": Main2(); break;
                default: Console.WriteLine("vale valik");break;
            }

        }

        static void Main1()
        {


            //int kaardiNumber = 30;
            //Mast mast = (Mast)(kaardiNumber / 13);
            //Kaart kaart = (Kaart)(kaardiNumber % 13 + 2);
            //Console.WriteLine($"{mast} {kaart}");

            int[] pakkEnne = Enumerable.Range(0, 52).ToArray();
            for (int i = 0; i < pakkEnne.Length; i++)
            {
                Console.Write($"{(Mast)(pakkEnne[i] / 13)} {(Kaart)(pakkEnne[i] % 13 + 2)}\t" + (i%4 == 3 ? "\n" : ""));
            }

            // ilmselt miskit randomit läheb vaja
            Random r = new Random(5138100);

            // 1. valid kaks juhuslikku kaarti ja vahetad need omavahel. Teed seda üsna mitu korda, 
            // nii et pakk saab "küllalt" segatud. (Selle mõtlesin täna)
            Console.WriteLine("\nPeale segamist");

            // 1. segamine
            for (int i = 0; i < 1000000; i++)
            {
                int yks = r.Next() % 52;
                int kaks = r.Next() % 52;
                int kaart = pakkEnne[yks];
                pakkEnne[yks] = pakkEnne[kaks];
                pakkEnne[kaks] = kaart;
            }

            for (int i = 0; i < pakkEnne.Length; i++)
            {
                Console.Write($"{(Mast)(pakkEnne[i] / 13)} {(Kaart)(pakkEnne[i] % 13 + 2)}\t" + (i % 4 == 3 ? "\n" : ""));
            }

            // 2. segamine
            List<int> enne = pakkEnne.ToList();
            List<int> siis = new List<int>();
            while (enne.Count > 0)
            {
                int nr = r.Next() % enne.Count;
                siis.Add(enne[nr]);
                enne.RemoveAt(nr);
            }
            pakkEnne = siis.ToArray();
            Console.WriteLine("\npeale teist segamist");
            for (int i = 0; i < pakkEnne.Length; i++)
            {
                Console.Write($"{(Mast)(pakkEnne[i] / 13)} {(Kaart)(pakkEnne[i] % 13 + 2)}\t" + (i % 4 == 3 ? "\n" : ""));
            }

            // jagame paki neljaks

            // NB! see ei ole AINUKE ega AINUÕIGE võimalus seda lahendada!

            // mul oleks vaja jagada pakk (52 kaarti) neljaks (igas 13 kaarti)
            // kas teha 2-mõõtmeline massiiv?
            // mul on kästud iga pakiga midagi veel teha - siis massiiv ei sobi

            // seepärast ma teen NELI listi (neli pakki)
            // ja lihtsuse mõttes siis 1-mõõtmelise massiivina

            List<int>[] pakid = new List<int>[4];
            // pakid on MASSIIV, mis koosneb List<int> -idest
            // ja seal on selliseid neli tükki (0..3)

            // need listid tuleb aga enne tekitada
            for (int i = 0; i < 4; i++)
            {
                pakid[i] = new List<int>();
            }

            // võiks na nii:
            List<int>[] pakid2 = 
                {
                    new List<int>(),
                    new List<int>(),
                    new List<int>(),
                    new List<int>()
                };

            for (int i = 0; i < pakkEnne.Length; i++)
            {
                pakid[i % 4].Add(pakkEnne[i]);
            }
            // vaatame, mis sai
            foreach (List<int> pakk in pakid)pakk.Sort();
                    
                


            Console.WriteLine("\nsorteeritud pakkidena");
            for (int i = 0; i < 13; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    Console.Write($"{(Mast)(pakid[j][i] / 13)} {(Kaart)(pakid[j][i] % 13 + 2)}\t" );

                }
                Console.WriteLine();
            }

        }
    }
}
