﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaardidMastidEnummid
{

    static partial class Program
    {
        static string ToKaart(this int kaart)
            => $"{(Mast)(kaart / 13)} {(Kaart)(kaart % 13 + 2)}";

        static void Main2()
        {
            Random r = new Random();
            int nr = 0;
            // tekitame segatud paki
            var segatudPakk = Enumerable.Range(0, 52)
                .OrderBy(x => r.NextDouble())
                .Select(x => new { nr = nr++, value = x })
                .OrderByDescending(x => x.value)
                .ToLookup(x => x.nr % 4);
            var pakid = Enumerable.Range(0, 4)
                .Select(x => segatudPakk[x].ToList())
                .ToArray();
            for (int i = 0; i < 13; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    Console.Write($"\t{pakid[j][i].value.ToKaart()}");
                }
                Console.WriteLine();
            }



        }
    }
}
